#include "libft.h"
#include <stdio.h>

size_t ft_intlen(int n)
{
	size_t i;

	i = 0;

	while (n > 0)
	{
		n = n / 10;
		i++;
	}
	return (i);
}
/*
char *ft_itoa(int n)
{
	int i;
	int rsc;
	char *retstr;
	
	i = 0;
	retstr = (char *)malloc(sizeof(n));

// make if statement to check negative then malloc size with ftlen

// use ftlen results as limit
// check for int max and min
	if (n < 0)
	{
		retstr[i] ='-';
		n = n * -1;
		i++;
	}
	while (n > 0)
		{
		if (n > 10)
		{
			retstr[i] = n % 10;
			i++;
			n = n / 10;
		}
		else
		{
			retstr[i] = n;
			n = n / 10;
		}
	}
	
	return (retstr);

}
*/
int main()
{
	int n;
	n = 304321;
	//printf("ft_itoa: |%s| \n", ft_itoa(n));
	printf("ft_intlen: |%zu| \n", ft_intlen(n));
	return (0);
}

