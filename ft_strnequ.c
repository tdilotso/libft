#include "libft.h"
/*


void    ft_print_result(int n)
{
        if (n == 0)
                write(1, "0", 1);
        else
                write(1, "1", 1);
}
*/

int     ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t c;

        if (s1 == NULL || s2 == NULL)
                return (0);
	if (ft_strlen(s1) != ft_strlen(s2))
		return (0);
	c = 0;
	while (s1[c] != '\0' && c < n)
	{
		if (s1[c] == s2[c])
			c++;
		else
			return(0);
	}
                return (1);
}
/*
int main()
{
	ft_print_result(ft_strnequ("tew", "tez", 2));
	return (0);
}*/
