/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 10:46:24 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 16:40:28 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t ft_strlcat(char *dst, const char *src, size_t dstsize)
{

	size_t idst;
	size_t c;
	size_t counter;

	
	c = 0;
	idst = 0;
//	if (dst[0] == '\0' || src == NULL)
//		return (dstsize);
	while (dst[idst] != '\0' && idst < dstsize)
	{	
		c++;
		idst++;
	}
	counter = 0;
	 while (src[counter] != '\0')
	 {
		 c++;
		 if (idst < dstsize)
		 {
			 dst[idst] = src[counter];
			 dst[1 + idst] = '\0';
		 }
		 counter++;
		 idst++;
		 if (idst == dstsize)
			 dst[--idst] = '\0';
	 }
	 //dst[--dstsize] = '\0';
		 return (c);
}
