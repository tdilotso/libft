/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/30 13:26:23 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 14:58:00 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strchr(const char *s, int c)
{
	size_t i;
	size_t l;
	unsigned char num;
	char *w;
	
	w = (char *)s;
	num = (unsigned char)c;
//	l = ft_strlen(s);
	i = 0;
	if (w[i] == c)
		return (w);
	else
		l = ft_strlen(w);
	while(i < l)
	{
		if(w[i] != num && i < l)
			i++;
		if(w[i] == num)
			return ((char *)(w + i));

	}
	return (NULL);
		
}
/*
int main()
{
	  const char str[] = "http://www.tutorialspoint.com";
   const char ch = 's';
   char *ret;

   ret = ft_strchr(str, ch);

   printf("String after |%c| is - |%s|\n", ch, ret);

   return(0);
}*/
