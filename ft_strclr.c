/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 11:34:01 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 16:22:39 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strclr(char *s)
{

	size_t w;
	size_t i;

	i = 0;
	if(s == NULL)
		return ;
	w = ft_strlen(s);

	if(!s)
	{
		return ;
	}
	
	if(s)
	{
	while(i < w)
	{
		s[i++] = '\0';
		//i++;
	}

	}
	while(i < w)
	{
		s[i++] = '\0';
		//i++;
	}
//	s[i] ='\0';
}
