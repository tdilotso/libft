/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 11:40:09 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 14:53:50 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strrchr(const char *s, int c)
{
	size_t i;
	size_t l;
	unsigned char num;
	char *w;
	
	w = (char *)s;
	num = (unsigned char)c;
//	l = ft_strlen(s);
	i = 0;
	if (w[i] == c)
		return (w);
	else
		l = ft_strlen(w);
	while(i < l)
	{
		if(w[l] != num && i < l)
			l--;
		if(w[l] == num)
			return ((char *)(w + l));

	}
	return (NULL);
		
}
