/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 12:50:28 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/31 16:17:58 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

int		ft_atoi(const char *str)
{
	int i;
	const char *st; 
	long counter;
	long sign;
	int coulen;

	st = str;
	i = 0;
	counter = 0;
	sign = 1;
	coulen = 0;
	while(str[i] == '\0' || str[i] == '\n' || str[i] == '\t' || str[i] == ' '  || str[i] == '\v' || str[i] == '\r' 
	|| str[i] == '\f') 
	{
		i++;
	}
	if(str[i] == '-')
	{
		sign = -1;
		i++;
	}
		while (ft_isdigit(str[i]) > 0)
		{
			counter = (counter * 10 + (str[i] - '0'));
			i++;
			coulen++;
		}
	if(coulen > 10 && sign < 0)
	{
		return(0);
	}
	if(coulen > 10 && sign > 0)
	{
		return(-1);
	}
		
	return (counter * sign) ;
}
/*
int	main()
{
	char *str1;
	char *str2;

	str1 = "AA5	4wwq we \nr1";
	str2 = "	 -12 553235";

	printf("%i\n", ft_atoi(str1));
	printf("%d", ft_atoi(str2));

	return(0);
}*/
