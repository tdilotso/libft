/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/25 11:07:01 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 15:35:01 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strstr(const char *haystack, const char *needle)
{
	int	i;
	int	j;
	int	hlen;
	int	nlen;
	int	counter;

	counter = 0;
	i = 0;
	j = 0;
	if (needle[i] == '\0')
		return((char *)haystack);
	hlen = ft_strlen(haystack);
	nlen = ft_strlen(needle);
	while (needle[j] != '\0')
	{
		if (ft_memchr(haystack,(int)needle[j],hlen) == NULL)
		{
			return ((char *)haystack);
		}
		else if (ft_memcmp(haystack + j, needle, nlen) == 0)
		{
			return ((char *)(haystack + j - 1));
		}
		j++;
	}
	return ((char *)(haystack + j));
}
