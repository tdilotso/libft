/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/24 12:47:02 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/31 14:18:42 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*str;
	str = (char *)malloc((size + 1)*sizeof(char));
	if (!str)
	{
		return (NULL);
	}
	else
	{
		ft_bzero(str, size+1);
	}
	return (str);
}
