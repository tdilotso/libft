#include "libft.h"

char    *ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
        char *tmp;
        size_t i;
        size_t slen;

        i = 0;
        if(!s)
        {
                return(NULL) ;
        }
        slen = (size_t)ft_strlen(s);

        if(!(tmp = (char *)malloc(slen + 1)))
                        return NULL;

        while(i < slen)
        {
                tmp[i] = f(i, s[i]);
                i++;
        }
        tmp[i] = '\0';
        return(tmp);
}

