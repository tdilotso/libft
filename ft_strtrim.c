/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/05 11:53:15 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/05 12:21:30 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strtrim(char const *s)
{
	char *trim;
	unsigned int i;
	size_t c;

	i = 0;
	c = 0;
	if (s == NULL)
	{
		trim = (char *)ft_strnew(0);
		return (trim);
	}
	c = ft_strlen(s);
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	while (s[c] == ' ' || s[c] == '\t' || s[c] == '\n' || s[c] == '\0')
		c--;
	return (ft_strnew( i, c));

}
