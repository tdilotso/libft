#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t dl = ft_strlen(dest);
	size_t i;

	i = 0;
	while (i < n && src[i] != '\0')
	{
		dest[dl+i] = src[i];
		i++;
	}
	dest[dl+i] = '\0';
	return (dest);
}
