#include "libft.h"

int	ft_strcmp(const char *s1, const char *s2)
{
	size_t i;
/*
	if (s1 == NULL || s2 == NULL)
		return 0;
	if (ft_strlen(s1) != ft_strlen(s2))
		return (-1);
	i = ft_strlen(s1);
		return  (ft_memcmp(s1, s2, i));
*/
	size_t c;
	unsigned char	*str1;
	unsigned char	*str2;
	i = 0;
	str1 = (unsigned char *)str1;
	str2 = (unsigned char *)str2;
	while (str1[i] != '\0' &&  str2[i] != '\0' && str1[i] == str2[i])
		i++;
	return (str1[i] - str2[i]);
}
