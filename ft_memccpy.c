/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 14:29:21 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/29 14:49:07 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "libft.h"

void *ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t		i;
	unsigned char	*sostr;
	unsigned char	*dstr;
	unsigned char	numC;

	i= 0;
	dstr = (unsigned char *)dst;
	sostr = (unsigned char *)src;
	numC = (unsigned char)c;

	while(i < n)
	{
		dstr[i] = sostr[i];
		if(sostr[i] == numC)
		{
			dstr[i] = numC;
			//dstr[i+1] = '\0';
			return (dstr + i + 1);
		}
		i++;
	}
	return (NULL);
}
