#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *sub;
	size_t i;

	i = 0;
	if (s == NULL || (int)len < 0)
		return (0);
	if (!(sub = malloc(len + 1)))
		return (NULL);
	while (i < len)
	{
		sub[i] = s[start];
		i++;
		start++;
	}
	sub[i] = '\0';
	return (sub);
}
