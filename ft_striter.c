/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 13:39:23 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/31 15:02:50 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

void	ft_striter(char *s, void (*f)(char*))
{
	if(!s)
	{
		return ;
	}
	while(*s != '\0' )
	{
		f(s);
		s++;
	}
}
/*
void ft_print(char *c)
{

	printf("%s ::: working", c);
}

int main()
{
	char *w = "hello world";

	ft_striter(w,ft_print);

	return (0);
}*/
