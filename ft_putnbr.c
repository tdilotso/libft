#include "libft.h"

void	ft_putnbr(int n)
{
	char s;

	s = (char)n;
	if (ft_isdigit(n))
	{
		write(1, &s, 1);
	}
	else
		return ;
}
