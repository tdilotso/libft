#include "libft.h"

t_list *ft_lstnew(void const *content, size_t content_size)
{
	t_list *newlst;
       	newlst = (t_list *)malloc(sizeof(t_list));
	if (!newlst)
		return (NULL);
	newlst->content = &content;
	newlst->content_size = &content_size;
	newlst->next = NULL;
	return (newlst);
}
