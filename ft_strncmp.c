/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/05 14:38:02 by tdilotso          #+#    #+#             */
/*   Updated: 2018/06/07 17:52:20 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t i;
//	size_t c;
	unsigned char	*str1;
	unsigned char	*str2;

	i = 0;	
	/*if (s1 == NULL || s2 == NULL)
		return 0;*/
	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	while ((str1[i] != '\0' &&  str2[i] != '\0' && str1[i] == str2[i]) || i < n)
	{
		i++;
	}
	return (str1[i] - str2[i]);
}

int	main()
{
	const char str1[] = "abcdefgh";
	const char str2[] = "abcdwxyz";

	printf("OG: |%i|\n", strncmp(str1, str2, 4));
	printf("myFT: |%i|\n", ft_strncmp(str1, str2, 4));
		return (0);
}
