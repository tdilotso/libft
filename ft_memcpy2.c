/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 13:26:11 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/29 13:31:31 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memcpy2(void *dst, const void *src, size_t n)
{
	int i;
	char *sostr;
	char *dstr;
	unsigned char num;

	i= 0;
	dstr = (char *)dst;
	sostr= (char *)src;
	num = (unsigned char)n;

	while (i < (num-1))
	{

		dstr[i] = sostr[i];
		i++;
	}
	return (dst);
}
