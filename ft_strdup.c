/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdilotso <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/23 12:56:48 by tdilotso          #+#    #+#             */
/*   Updated: 2018/05/30 09:39:39 by tdilotso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{	
	const char	*oldStr;
	char		*dup;
	int		sl;

	oldStr = s1;
	sl = ft_strlen(oldStr);
	dup = ft_memalloc(sl + 1);
	if (dup == 0)
		return(0);
	ft_strcpy(dup, oldStr);
	return (dup);
}
